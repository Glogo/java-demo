package introduction;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * 参数化测试运行器测试
 * @author glogo
 *
 */

//第一步，先加上注解
@RunWith(value=Parameterized.class)
public class ParameterizedTest {

	//第二步，声明测试用例中要使用的变量
	private double expected;
	private double valueOne;
	private double valueTwo;
	
	
	public ParameterizedTest(double expected, double valueOne, double valueTwo){
		this.expected = expected;
		this.valueOne = valueOne;
		this.valueTwo = valueTwo;
	}
	
	//第三步，定义一个用@parameters注释的方法,这个方法的签名无须参数
	@Parameters
	public static Collection<Integer[]> getTestParameters(){
		return Arrays.asList(new Integer[][]{
				{2, 1, 1},
				{3, 2, 1},
				{4, 3, 1}
		});
	}
	
	
	@Test
	public void sum(){
		Calculator cal = new Calculator();
		Assert.assertEquals(expected, cal.add(valueOne, valueTwo), 0);	//这里相当于调用三次assertEquals()方法
	}
	
}
