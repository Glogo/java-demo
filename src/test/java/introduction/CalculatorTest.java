package introduction;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 测试类
 * @author glogo
 *
 */
public class CalculatorTest {

	
	@Test
	public void testAdd(){
		int[] A = {1,2};
		int[] B = {1,2};
		Calculator cal = new Calculator();
		double result = cal.add(1, 1);
		assertEquals(2, result, 0);		//相当于equals()方法
		assertArrayEquals("message", A, B);	//断言A和B数组相等
//		assertSame("message", A, B);		//断言A对象与B对象相同
		assertTrue("message", A!=null);		//判断第二个参数条件是否为空
		assertNotNull("message", A);		//判断A对象不为null
	}
}
