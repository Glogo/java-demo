package controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author glogo
 *
 */
public class TestDefaultController {

	private DefaultController controller;
	private Request request;
	private RequestHandler handler;
	
	private class SimpleRequest implements Request{
		private static final String DEFAULT_NAME = "Test";
		private String name;
		public SimpleRequest(String name) {
			this.name = name;
		}
		public SimpleRequest() {
			this(DEFAULT_NAME);
		}
		@Override
		public String getName() {
			return this.name;
		}
	}
	
	private class SimpleHandler implements RequestHandler{
		@Override
		public Response process(Request request) throws Exception {
			return new SimpleResponse();
		}
	}
	
	private class SimpleExceptionHandler implements RequestHandler{

		@Override
		public Response process(Request request) throws Exception {
			throw new Exception("error processing request!");
		}
	}
	
	private class SimpleResponse implements Response{
		private static final String NAME = "Test";
		public String getName(){	return NAME;	}
		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if(obj instanceof SimpleResponse){
				result = ((SimpleResponse) obj).getName().equals(getName());
			}
			return result;
		}
		@Override
		public int hashCode() {
			return NAME.hashCode();
		}
	}
	
	
	
	@Before
	public void instantiate() throws Exception{
		controller = new DefaultController();
		Request request = new SimpleRequest();
		RequestHandler handler = new SimpleHandler();
		controller.addHandler(request, handler);
	}
	
	@Test
	public void testAddHandler(){
		RequestHandler handler2 = new SimpleHandler();
		Assert.assertSame("Handler we set in controller should be the same handler we get", 
				handler2, handler);
	}
	
	@Test
	public void testProcessRequest(){
		Response response = controller.processRequest(request);
		Assert.assertNotNull("Must not null a response" ,response);
//		Assert.assertEquals("Response should be the same type", SimpleResponse.class, response.getClass());
		Assert.assertEquals(new SimpleResponse(), response);
//		Assert.assertTrue(new SimpleResponse() instanceof SimpleResponse);
	}
	
	@Test
	public void testProcessRequestAnswerErrorResponse(){
//		SimpleRequest request = new SimpleRequest();	//�������д��addHandler�ᱨ�?��Ϊ֮ǰ��Test�����Ѿ���ӹ���ͬ���requestHandler
		SimpleRequest request = new SimpleRequest("testError");	//����HashMap�Ľ�����newһ���µĲ�һ���request
		SimpleExceptionHandler handler = new SimpleExceptionHandler();
		controller.addHandler(request, handler);	
		Response response = controller.processRequest(request);
		Assert.assertNotNull("Must not a null response", response);
		Assert.assertEquals(ErrorResponse.class, response.getClass());
	}
	
	
	@Test(expected=RuntimeException.class)
	public void testGetHandlerNotDefined(){
		SimpleRequest request = new SimpleRequest("testNotDefined");
		controller.getHandler(request);
	}
	
	@Test(expected=RuntimeException.class)
	public void testAddRequestDuplicateName(){
		SimpleRequest request = new SimpleRequest();
		SimpleExceptionHandler handler = new SimpleExceptionHandler();
		controller.addHandler(request, handler);
	}
	
	//���Լ���ע����Ե������Ԫ����
	@Ignore
	@Test(timeout=130)
	public void testProcessMutipleRequestTimeout(){
		Request request;
		Response response = new SimpleResponse();
		RequestHandler handler = new SimpleHandler();
		for(int i = 0; i < 99999; i++){
			request = new SimpleRequest(String.valueOf(i));
			controller.addHandler(request, handler);
			response = controller.processRequest(request);
			Assert.assertNotNull(response);
			Assert.assertNotSame(ErrorResponse.class, response.getClass());
		}
		
	}
}


