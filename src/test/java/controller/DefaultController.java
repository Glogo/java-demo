package controller;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;



public class DefaultController implements Controller{

	private Map<String, RequestHandler> requestHandlers = new HashMap<>();

	public RequestHandler getHandler(Request request){
		if(!this.requestHandlers.containsKey(request.getName())){
			String message = "Can not find handler for request name [" + request.getName() + "]";
			throw new RuntimeException(message);
		}
		return (RequestHandler)this.requestHandlers.get(request.getName());
	}
	

	@Override
	public Response processRequest(Request request) {
		Response response;
		try{
			response = getHandler(request).process(request);
		}catch(Exception ex){
			response = new ErrorResponse(request, ex);
		}
		return response;
	}

	@Override
	public void addHandler(Request request, RequestHandler requestHandler) {
		if(this.requestHandlers.containsKey(request.getName())){
			throw new RuntimeException("A request handler has already been registered for request name"
					+ "[" + request.getName() + "]");
		}else{
			this.requestHandlers.put(request.getName(), requestHandler);
		}
	}
	
	
	
}
