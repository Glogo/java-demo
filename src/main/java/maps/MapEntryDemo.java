package maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class MapEntryDemo {

	public static void main(String[] args) {
		Map<String,String> map=new HashMap();
		map.put("1", "aaaa");
        map.put("2", "bbbb");
        map.put("3", "cccc");
        System.out.println("");
        Iterator it = map.entrySet().iterator();
        while(it.hasNext()){
        	Entry<String, String> entries = (Entry<String, String>) it.next();
        	System.out.println("key:" + entries.getKey() + " value:" + entries.getValue());
        }

        System.out.println("");
        for (Entry<String, String> m : map.entrySet()) {
        	System.out.println("key:"+m.getKey()+" value:"+m.getValue());    
        }
	}
}
