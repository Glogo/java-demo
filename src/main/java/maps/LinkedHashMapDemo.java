package maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

/*
 * @author glogo
 *
 */
public class LinkedHashMapDemo {

	public static void main(String[] args) {
		HashMap<String, String> hasmap = new HashMap<>(20);
		hasmap.put("", "");
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>(10, 0.75f, true);
		map.put("1", "aa");
		map.put("2", "bb");
		map.put("3", "cc");
		map.put("4", "dd");
		map.get("3");
		for(Iterator<String> it = map.keySet().iterator(); it.hasNext();){
			String name = it.next();
//			System.out.println(map.get(name));
			System.out.println(name);
		}
	}
}
