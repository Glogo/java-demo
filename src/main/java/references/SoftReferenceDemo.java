package references;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;

/**
 * Created by Glogo on 2014/8/25.<br>
 * 软引用，是强度仅次于强引用的引用类型
 * 一个持有软引用的对象，不会被JVM很快回收，JVM会根据当前堆的使用情况来判断何时回收
 * 软引用可以用于实现内存敏感的Cache
 *
 * -Xmx4M -XX:+PrintGCDetails
 */
public class SoftReferenceDemo {

    public static void main(String[] args){
        MyObject myObject = new MyObject();

        ReferenceQueue<MyObject> refQueue = new ReferenceQueue<>();
        SoftReference<MyObject> softRef = new SoftReference<>(myObject, refQueue);
        new CheckRefQueue(refQueue).start();

        myObject = null; //删除强引用

        System.out.println("Before GC:softRef Get = " + softRef.get());

        System.gc();
        System.out.println("分配大内存块");
        byte[] bigArray = new byte[1024*2*1024];
        System.out.println("After GC:softRef Get = " + softRef.get());

    }

}
