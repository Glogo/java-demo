package references;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * Created by Glogo on 2014/8/25.
 * 弱引用是比软引用强度更低的引用，当发生GC时，只有发现弱引用就会被系统回收，不管对空间是否足够
 * -Xmx5M -Xms3M -XX:+PrintGCDetails
 */
public class WeakReferenceDemo {

    public static void main(String[] args){
        MyObject myObject = new MyObject();

        ReferenceQueue<MyObject> refQueue = new ReferenceQueue<>();
        WeakReference<MyObject> weakRef = new WeakReference<>(myObject, refQueue);
        new CheckRefQueue(refQueue).start();

        myObject = null; //删除强引用

        System.out.println("Before GC:weakRef Get = " + weakRef.get());

        System.gc();
        System.out.println("After GC:weakRef Get = " + weakRef.get());
    }

}
