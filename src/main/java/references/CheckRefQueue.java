package references;


import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

public class CheckRefQueue extends Thread{

    private ReferenceQueue<MyObject> refQueue;

    public CheckRefQueue(ReferenceQueue<MyObject> softQueue){
        this.refQueue = softQueue;
    }

    @Override
    public void run() {
        try {
            Reference<MyObject> obj = (Reference<MyObject>) refQueue.remove();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
