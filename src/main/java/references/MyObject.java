package references;

/**
 * Created by Glogo on 2014/8/25.<br>
 * 这个包下的类主要关于Java引用类型
 * 引用对象
 */
public class MyObject {

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("MyObject's finalized called");
    }

    @Override
    public String toString() {
        return "I am MyObject.";
    }
}
