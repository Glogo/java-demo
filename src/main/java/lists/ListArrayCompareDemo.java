package lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Created by Glogo on 2014/8/25.<br/>
 * 比较三种常用List实现，它们方法的效率
 *
 */
public class ListArrayCompareDemo {

    private static ArrayList<Object> objs1 = new ArrayList<>();
    private static  LinkedList<Object> objs2 = new LinkedList<>();
    private static Vector<Object> objs3 = new Vector<>();

    public static void main(String[] args){
        ListArrayCompareDemo.AddCompare();
        ListArrayCompareDemo.IteratorCompare();
    }


    /**
     * 比较遍历方法的效率
     */
    public static void IteratorCompare(){
        //ArrayList
        Iterator<Object> it1 = objs1.iterator();
        long startTime = System.currentTimeMillis();
        while(it1.hasNext()){
            it1.next();
        }
        System.out.println("ArrayList.iterator " + (System.currentTimeMillis() - startTime) + "ms");

        //LinkedList
        Iterator<Object> it2 = objs2.iterator();
        startTime = System.currentTimeMillis();
        while(it2.hasNext()){
            it2.next();
        }
        System.out.println("LinkedList.iterator " + (System.currentTimeMillis() - startTime) + "ms");

        //Vector
        Iterator<Object> it3 = objs3.iterator();
        startTime = System.currentTimeMillis();
        while(it3.hasNext()){
            it3.next();
        }
        System.out.println("Vector.iterator " + (System.currentTimeMillis() - startTime) + "ms");
    }

    /**
     * 比较add方法的效率
     */
    public static void AddCompare(){
        //arraylist add
        long startTime = System.currentTimeMillis();
        for(int i = 0; i < 5000000; i++){
            objs1.add(i);
        }
        System.out.println("ArrayList.add " + (System.currentTimeMillis() - startTime) + "ms");

        //linked list
        startTime = System.currentTimeMillis();
        for(int i = 0; i < 5000000; i++){
            objs2.add(i);
        }
        System.out.println("LinkedList.add " + (System.currentTimeMillis() - startTime) + "ms");

        //vector
        startTime = System.currentTimeMillis();
        for(int i = 0; i < 5000000; i++){
            objs3.add(i);
        }
        System.out.println("Vector.add " + (System.currentTimeMillis() - startTime) + "ms");
    }



}
