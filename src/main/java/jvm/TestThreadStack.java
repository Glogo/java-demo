package jvm;

/**
 * 测试Java虚拟机线程栈
 * -Xss1M 每个线程占用1M栈空间
 * -Xmx100M 最大堆内存100M
 * -Xms100M 最小堆内存100M
 */
public class TestThreadStack {

	public static class MyThread extends Thread{
		@Override
		public void run() {
			try{
                System.out.println("thread:" + this.getId());
				Thread.sleep(5000);
			}catch(InterruptedException ie){
				ie.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		int i = 0;
		try{
			for(; i < 100000; i++){
				MyThread mt = new MyThread();
                mt.start();
			}
		}catch(OutOfMemoryError e){
			System.out.println("count thread is " + i);
			e.printStackTrace();
		}
	}
	
}
