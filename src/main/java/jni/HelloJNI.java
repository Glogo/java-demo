package jni;

/**
 * 暂时不要测试
 */
public class HelloJNI {

	static{
		//这里的so文件路径：/usr/java/jdk1.7.0_21/jre/lib/i386/client/libhello.so
		System.loadLibrary("hello");
	}
	
	public native void sayHello();
	
	
	public static void main(String[] args) {
		System.out.println(System.getProperty("java.library.path"));
		new HelloJNI().sayHello();
		
	}
}
