package network.serialiable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 这样会导致栈溢出
 * @author glogo
 *
 */
public class StringList implements Serializable{

	private int size = 0;
	private Node head = null;
	private Node end = null;
	
	private static class Node implements Serializable{
		String data;
		Node next;
		Node prev;
	}
	
	/**
	 * 在链表末尾加上
	 * @param data
	 */
	public void add(String data){
		Node node = new Node();
		node.data = data;
		node.next = null;
		node.prev = end;
		if(end != null) end.next = node;
		end = node;
		size++;
		if(size == 1) head = end;
	}
	
	
	public static void main(String[] args) throws IOException{
		
		StringList sl = new StringList();
		for(int i = 0; i < 15000; i++){
			sl.add("hello" + i);
		}
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(buf);
		oos.writeObject(sl);
	}
}
