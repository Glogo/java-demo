package network.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GetKey {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/STOREDB";
		String user = "root";
		String password = "123456";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, password);
			
			Statement stmt = conn.createStatement();
			stmt.execute("insert into CUSTOMERS(NAME,AGE,ADDRESS) values('丁乙',34,'广东')", Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next())
				System.out.println(rs.getInt(1));
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
