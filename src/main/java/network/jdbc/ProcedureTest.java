package network.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class ProcedureTest {

	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/STOREDB",
					"root",
					"123456");
			CallableStatement callStmt = conn.prepareCall("{call demoSP(?,?)}");
			callStmt.setString(1, "Tom");
			//注册第二个参数的类型
			callStmt.registerOutParameter(2, Types.INTEGER);
			callStmt.setInt(2, 1);
			
			if(callStmt.execute()){
				ResultSet rs = callStmt.getResultSet();
				while(rs.next()){
					System.out.println(rs.getString(1));
				}
			}
			
			int inoutParam = callStmt.getInt(2);
			System.out.println(inoutParam);
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
}
