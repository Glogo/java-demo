package network.jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BlobTest {

	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/STOREDB",
					"root",
					"123456");
			PreparedStatement pStmt = conn.prepareStatement(
					"insert into blob_clob_test(id,img_file) values(?,?)");
			pStmt.setInt(1, 1);
			
			FileInputStream fin = new FileInputStream("/home/glogo/图片/Java内存.jpg");
			pStmt.setBinaryStream(2, fin);
			
			
			pStmt.executeUpdate();
			
			fin.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
