package network.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LotsDataInsertion {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/STOREDB";
		String user = "root";
		String password = "123456";
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url, user, password);
			conn.setAutoCommit(false);
			
			String sql = "insert into CUSTOMERS(NAME,AGE,ADDRESS) values(?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			long startTime = System.currentTimeMillis();
			for(int i = 1; i <= 100000;i++){
				pStmt.setString(1, "李雷"+i);
				pStmt.setInt(2, i);
				pStmt.setString(3, "西藏"+i);
				pStmt.execute();
//				pStmt.addBatch();
//				if(i%1000==0)
//				{
//					pStmt.executeBatch();
//					conn.commit();
//					pStmt.clearBatch();
//				}
				
			}
		
//			pStmt.executeBatch();
			conn.commit();
			long endTime = System.currentTimeMillis();
			System.out.println("pStmt Time =" + (endTime - startTime) + "ms");
			
			
			pStmt.close();
			conn.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
}
