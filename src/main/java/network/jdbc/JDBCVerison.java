package network.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCVerison {

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/STOREDB",
					"root",
					"123456");
			DatabaseMetaData dbMeta = conn.getMetaData();
			System.out.println(dbMeta.getJDBCMajorVersion()+"."+dbMeta.getJDBCMinorVersion());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
