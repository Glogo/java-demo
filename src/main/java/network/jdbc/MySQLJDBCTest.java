package network.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLJDBCTest {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/STOREDB";
		String user = "root";
		String password = "123456";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.setLogWriter(new PrintWriter(System.out, true));
			Connection conn = DriverManager.getConnection(url, user, password);
			
//			System.out.println(conn.getMetaData().getMaxStatements());
			
			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
//			stmt.executeUpdate("insert into CUSTOMERS(ID,NAME,AGE,ADDRESS)" +
//					" values(4,'王芳',24,'江苏')");
			
			
			ResultSet res = stmt.executeQuery("select * from CUSTOMERS where AGE > 20");
			
			while(res.next()){
				long id = res.getLong(1);
				String name = res.getString(2);
				int age = res.getInt(3);
				String address = res.getString(4);
				
				System.out.println("id="+id+",name="+name+",age="+age+",address="+address);
			}
			switch(res.getType()){
			case ResultSet.TYPE_FORWARD_ONLY:
				System.out.print("TYPE_FORWARD_ONLY=");	break;
			case ResultSet.TYPE_SCROLL_INSENSITIVE:
				System.out.print("TYPE_SCROLL_INSENSITIVE=");	break;
			case ResultSet.TYPE_SCROLL_SENSITIVE:
				System.out.print("TYPE_SCROLL_SENSITIVE=");	break;
			}
			System.out.println(res.getType());
			
			switch(res.getConcurrency()){
			case ResultSet.CONCUR_READ_ONLY:
				System.out.print("CONCUR_READ_ONLY=");	break;
			case ResultSet.CONCUR_UPDATABLE:
				System.out.print("CONCUR_UPDATABLE=");	break;
			}
			System.out.println(res.getConcurrency());
			
			res.close();
			stmt.close();
			conn.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("ErrorCode=" + e.getErrorCode());
			System.out.println("SQLState=" + e.getSQLState());
			System.out.println("Reason=" + e.getMessage());
			e.printStackTrace();
		}
		
	}
	
}
