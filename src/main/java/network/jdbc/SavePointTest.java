package network.jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class SavePointTest {

		public static void main(String[] args) {
			Connection conn = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/STOREDB",
						"root",
						"123456");
				conn.setAutoCommit(false);
				
				Statement stmt = conn.createStatement();
				stmt.execute("delete from ACCOUNTS");
				stmt.execute("insert into ACCOUNTS(ID,NAME,BALANCE)" +
						" values(1,'Tom',1000)");
				Savepoint p1 = conn.setSavepoint();
				stmt.execute("update ACCOUNTS set BALANCE=900 where ID=1");
				conn.rollback(p1);
				stmt.execute("insert into ACCOUNTS(ID,NAME,BALANCE)" +
						" values(2,'Jack',2000)");
				
				conn.commit();
				
				conn.close();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			} 
			
	}
	
}
