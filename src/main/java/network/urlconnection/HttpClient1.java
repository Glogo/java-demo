package network.urlconnection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class HttpClient1 {

	public static void main(String[] args) {
		try{
			URL url = new URL("http://www.javathinker.org");	//注意要加上协议
			
			//接收响应结果
			InputStream is = url.openStream();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buff = new byte[1024];
			int len = -1;
			
			while((len = is.read(buff)) != -1){
				bos.write(buff, 0, len);
			}
			
			System.out.println(new String(bos.toByteArray()));
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
}
