package network.urlconnection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class HttpClient2 {

	public static void main(String[] args) {
		
		try{
			URL url = new URL("http://www.baidu.com");
			URLConnection urlConnection = url.openConnection();
			urlConnection.setDoInput(true);
			System.out.println("Content:" + urlConnection.getContent());
			System.out.println("正文类型：" + urlConnection.getContentType());
			System.out.println("正文长度：" + urlConnection.getContentLength());
			System.out.println("文本编码：" + urlConnection.getContentEncoding());
			System.out.println("响应码：" + urlConnection.getHeaderField("Server"));
//			System.out.println("猜测类型：" + URLConnection.guessContentTypeFromName("index.tar"));
			
			InputStream in = urlConnection.getInputStream();
			
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buff = new byte[1024];
			int len = -1;
//			
			while((len = in.read(buff)) != -1){
				bos.write(buff, 0, len);
			}
			
			System.out.println(new String(bos.toByteArray()));
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
}
