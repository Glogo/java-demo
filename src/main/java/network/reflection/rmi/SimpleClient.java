package network.reflection.rmi;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class SimpleClient {

	public void invoke() throws IOException, ClassNotFoundException{
		Socket socket = new Socket("localhost", 8080);
		OutputStream os = socket.getOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(os);
		
		InputStream is = socket.getInputStream();
		ObjectInputStream ois = new ObjectInputStream(is);
		
		Call call = new Call("network.reflection.rmi.HelloService", "echo",
				new Class[]{String.class}, new Object[]{"Hello RMI"});
		
		oos.writeObject(call);
		
		call = (Call)ois.readObject();
		System.out.println(call.getResult());
		
		ois.close();
		oos.close();
		socket.close();
	}
	
	public static void main(String[] args) throws Exception{
		new SimpleClient().invoke();
	}
}
