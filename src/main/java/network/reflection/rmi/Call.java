package network.reflection.rmi;

import java.io.Serializable;

/**
 * 在远程方法调用中使用反射机制
 * @author glogo
 *
 */
public class Call implements Serializable{

	private String className;
	private String methodName;
	private Class[] paramsType;
	private Object[] params;
	
	private Object result;	//正确的结果为字符串，出错就为Exception
	
	public Call(String string, String string2, Class[] classes, Object[] objects) {
		this.className = string;
		this.methodName = string2;
		this.paramsType = classes;
		this.params = objects;
	}
	
	@Override
	public String toString() {
		return "className:" + className + "," + "methodName:" + methodName + "," + paramsType + "," + params;
	}
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public Class[] getParamsType() {
		return paramsType;
	}
	public void setParamsType(Class[] paramsType) {
		this.paramsType = paramsType;
	}
	public Object[] getParams() {
		return params;
	}
	public void setParams(Object[] params) {
		this.params = params;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
}
