package network.reflection.rmi;

public class HelloServiceImpl implements HelloService{

	@Override
	public String echo(String msg) {
		return "echo:" + msg;
	}

}
