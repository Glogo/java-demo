package network.reflection.rmi;

public interface HelloService {

	public String echo(String msg);
}
