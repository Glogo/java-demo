package network.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServer {

	public static void main(String[] args) {
		if(System.getSecurityManager() == null){
			System.setSecurityManager(new RMISecurityManager());
		}
		
		try{
			//创建远程对象
			HelloService service1 = new HelloServiceImpl("service1");
			//启动注册表,这样就不用在命令行下启动了
			LocateRegistry.createRegistry(1099);	//1099是RMI服务端的默认端口
			//绑定远程服务对象
			Naming.rebind("rmi://localhost:1099/HelloService1", service1);
			
			System.out.println("RMI服务器正在运行...");
		}catch(RemoteException e){
			e.printStackTrace();
		}catch(MalformedURLException e1){
			e1.printStackTrace();
		}
		
	}
	
}
