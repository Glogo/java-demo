package network.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

/**
 * 远程类
 * 构造方法必须抛出RemoteException
 * @author glogo
 *
 */
public class HelloServiceImpl extends UnicastRemoteObject implements HelloService{

	private String name;
	
	public HelloServiceImpl(String name) throws RemoteException{
		this.name = name;
//		super();
	}

	@Override
	public String echo(String msg) throws RemoteException {
		System.out.println(name + ":调用echo方法");
		return "echo:" + msg + " from " + name;
	}

	@Override
	public Date getTime() throws RemoteException {
		System.out.println(name + "：调用了getTime方法");
		return new Date();
	}
}
