package network.rmi;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;

public class RMIClient {

	public static void showRemoteObjects() throws Exception{
		Context nameContext = new InitialContext();
		NamingEnumeration<NameClassPair> e = nameContext.list("rmi:");
		while(e.hasMore()){
			System.out.println(e.next().getName());
		}
	}
	
	public static void main(String[] args) throws Exception{
		
		if(System.getSecurityManager() == null){
			System.setSecurityManager(new RMISecurityManager());
		}
		
		try{
			//获得远程对象的存根
			HelloService hello = (HelloService) Naming.lookup("rmi://localhost:1099/HelloService1");
			System.out.println(hello.echo("我是RMI客户端"));
			Class stubType = hello.getClass();
			System.out.println("hello是" + stubType.getName() + " 的实例");
			Class[] interfaces = stubType.getInterfaces();
			for(int i = 0; i < interfaces.length; i++){
				System.out.println("存根类实现了 " + interfaces[i].getName() + " 接口");
			}
			System.out.println(hello.getTime());
		}catch(Exception e){
			e.printStackTrace();
		}
		showRemoteObjects();
	}
}
