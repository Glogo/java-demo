package network.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

/**
 * 符合远程接口的条件
 * 1.直接或间接的继承Remote接口
 * 2.所有接口声明的方法均抛出RemoteException
 * @author glogo
 *
 */
public interface HelloService extends Remote{

	public String echo(String msg) throws RemoteException;
	
	public Date getTime() throws RemoteException;
	
}
