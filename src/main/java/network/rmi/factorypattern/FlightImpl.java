package network.rmi.factorypattern;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class FlightImpl extends UnicastRemoteObject implements Flight{

	private String flightNumber;
	private String origin;
	private String destination;
	private String skdDeparture;
	private String skdArrival;
	
	public FlightImpl(String aFlightNumber, String aOrigin, String aDestination,
			String aSkdDeparture, String aSkdArrival) throws RemoteException{
		this.flightNumber = aFlightNumber;
		this.origin       = aOrigin;
		this.destination  = aDestination;
		this.skdDeparture = aSkdArrival;
		this.skdArrival   = aSkdArrival;
	}

	@Override
	public String getFlighNumber() throws RemoteException {
		System.out.println("调用getFlightNumber()方法，返回 " + flightNumber);
		return flightNumber;
	}

	@Override
	public String getOrigin() throws RemoteException {
		return origin;
	}

	@Override
	public String getDestination() throws RemoteException {
		return destination;
	}

	@Override
	public String getSkdDeparture() throws RemoteException {
		return skdDeparture;
	}

	@Override
	public String getSkdArrival() throws RemoteException {
		return skdArrival;
	}

	@Override
	public void setOrigin(String origin) throws RemoteException {
		this.origin = origin;
	}

	@Override
	public void setDestination(String destination) throws RemoteException {
		this.destination = destination;
	}

	@Override
	public void setSkdDeparture(String sdkDeparture) throws RemoteException {
		this.skdDeparture = sdkDeparture;
	}

	@Override
	public void setSkdArrival(String sdkArrival) throws RemoteException {
		this.skdArrival = sdkArrival;
	}

	
	
	
}
