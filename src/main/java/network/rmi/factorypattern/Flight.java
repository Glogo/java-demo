package network.rmi.factorypattern;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 远程接口
 * @author glogo
 *
 */
public interface Flight extends Remote{

	public String getFlighNumber() throws RemoteException;
	
	public String getOrigin() throws RemoteException;
	
	public String getDestination() throws RemoteException;
	
	public String getSkdDeparture() throws RemoteException;
	
	public String getSkdArrival() throws RemoteException;
	
	
	public void setOrigin(String origin) throws RemoteException;
	
	public void setDestination(String destination) throws RemoteException;
	
	public void setSkdDeparture(String sdkDeparture) throws RemoteException;
	
	public void setSkdArrival(String sdkArrival) throws RemoteException;
	
}
