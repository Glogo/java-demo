package network.rmi.factorypattern;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;

public class FlightFactoryImpl extends UnicastRemoteObject implements FlightFactory{

	private Hashtable<String, Flight> flights;
	
	public FlightFactoryImpl() throws RemoteException{
		flights = new Hashtable<String, Flight>();
	}
	
	@Override
	public Flight getFlight(String flighNumber) throws RemoteException {
		
		Flight flight = flights.get(flighNumber);
		if(flight != null)	return flight;
		
		flight = new FlightImpl(flighNumber, null, null, null, null);
		flights.put(flighNumber, flight);
		return flight;
	}
	
}
