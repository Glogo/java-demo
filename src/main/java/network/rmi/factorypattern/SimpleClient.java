package network.rmi.factorypattern;

import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class SimpleClient {

	public static void main(String[] args) {
		
		String url = "rmi://192.168.1.100:1099/";
		
		try {
			Context namingContext = new InitialContext();
			FlightFactory factory = (FlightFactory) namingContext.lookup(url + "FlightFactory");
			
			Flight flight1 = factory.getFlight("679");
			flight1.setOrigin("ShangHai");
			flight1.setDestination("GuangZhou");
			
			System.out.println(flight1.getOrigin());
			System.out.println(flight1.getDestination());
			
			Flight flight2 = factory.getFlight("679");
			System.out.println(flight1 == flight2);	//false客户端每次访问同一个远程对象都会生成新的存根对象
			
			System.out.println(flight1.getClass().getName());
			
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
	}
	
}
