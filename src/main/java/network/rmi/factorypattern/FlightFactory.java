package network.rmi.factorypattern;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FlightFactory extends Remote{

	public Flight getFlight(String flighNumber) throws RemoteException;
	
}
