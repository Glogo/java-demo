package network.rmi.factorypattern;

import java.rmi.RemoteException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class SimpleServer {

	
	public static void main(String[] args) {
		
		try {
			
			System.setProperty("java.rmi.server.hostname", "192.168.1.100");
			
			FlightFactory flightFactory = new FlightFactoryImpl();
			
			Context namingContext = new InitialContext();

			namingContext.rebind("rmi://192.168.1.100:1099/FlightFactory", flightFactory);
			
			System.out.println("服务器注册了一个FlightFactory对象");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
	}
}
