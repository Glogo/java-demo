package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Glogo on 2014/8/30.
 *
 * 与Sender类对应
 *
 * Windows下程序的表现与linux表现不一样
 */
public class Receiver {

    private int port = 8000;
    private ServerSocket serverSocket;

    public static int stopWay = 1;    //结束通信的方式
    private final int NATURAL_STOP = 1; //自然结束
    private final int SUDDEN_STOP  = 2; //突然终止程序
    private final int SOCKET_STOP  = 3; //关闭Socket，再结束程序
    private final int OUTPUT_STOP  = 4; //先关闭输出流，再结束程序
    private final int SERVERSOCKET_STOP = 5;

    public Receiver() throws IOException {
        serverSocket = new ServerSocket(port);
        System.out.println("服务器已启动");
    }

    private BufferedReader getReader(Socket socket) throws IOException {
        InputStream is = socket.getInputStream();
        return new BufferedReader(new InputStreamReader(is));
    }

    public void receive() throws Exception {
        Socket socket = serverSocket.accept();
        BufferedReader br = getReader(socket);
        for(int i = 0; i < 20; i++){
            String msg = br.readLine();
            System.out.println("receive:" + msg);
            //下面这些都是未发送完就关闭
            if(i == 2){
                if (stopWay == SUDDEN_STOP){
                    System.out.println("突然终止程序");
                    System.exit(0);
                }else if (stopWay == SOCKET_STOP){
                    System.out.println("关闭Socket并终止程序");
                    socket.close();
                    break;
                }else if (stopWay == OUTPUT_STOP){
                    System.out.println("关闭接收流");
                    socket.shutdownOutput();
                    break;
                }else if (stopWay == SERVERSOCKET_STOP){
                    System.out.println("关闭ServerSocket并终止程序");
                    serverSocket.close();
                    break;
                }
            }
        }
        if (stopWay == NATURAL_STOP){
            socket.close();
            serverSocket.close();
        }
    }


    public static void main(String[] args) throws Exception {
        if(args.length > 0)
            stopWay = Integer.parseInt(args[0]);
        new Receiver().receive();
    }

}
