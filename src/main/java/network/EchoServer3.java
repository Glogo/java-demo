package network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.charset.Charset;

/**
 * 混用阻塞与非阻塞模式
 * @author glogo
 * 将接受线程与读写线程分开
 */
public class EchoServer3 {

	private Selector selector;
	private ServerSocketChannel ssc;
	private int port = 8000;
	private Charset charset = Charset.forName("UTF-8");
	
	public EchoServer3() throws IOException{
		selector = Selector.open();
		ssc = ServerSocketChannel.open();
		ssc.socket().setReuseAddress(true);
		ssc.socket().bind(new InetSocketAddress(port));
		System.out.println("服务器启动");
	}
	
	/**
	 * 接受线程
	 */
	public void accept(){
		
	}
	
	/**
	 * 读写线程
	 */
	public void service(){
		
	}
	
	public static void main(String[] args) throws IOException{
//		final EchoServer3 echoServer = new EchoServer3();
//		Thread acceptThread = new Thread(){
//			@Override
//			public void run() {
//				echoServer.accept();
//			}
//		};
//		acceptThread.start();
//		echoServer.service();
		
		System.out.println(Runtime.getRuntime().availableProcessors());
		
	}
	
	
}
