package network.httpserver;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Request {

	static class Action{
		private String name;
		public Action(String name) {this.name = name; }
		public String toString() {return name; }
		static Action GET = new Action("GET");
		static Action PUT = new Action("PUT");
		static Action POST = new Action("POST");
		static Action HEAD = new Action("HEAD");
		
		public static Action parse(String s){
			if(s.equals("GET"))	return GET;
			if(s.equals("PUT")) return PUT;
			if(s.equals("POST")) return POST;
			if(s.equals("HEAD")) return HEAD;
			throw new IllegalArgumentException(s);
		}
	}
	
	private Action action;
	private String version;
	private URI uri;
	
	public Action action(){ return action; };
	public String version(){ return version; }
	public URI uri(){ return uri; }
	
	private Request(Action a, String v, URI u){
		this.action = a;
		this.version = v;
		this.uri = u;
	}
	
	@Override
	public String toString() {
		return action + " " + version + " " + uri;
	}
	
	private static Charset requestCharset = Charset.forName("UTF-8");
	
	/**
	 * 判断HTTP请求是否包含了所有数据
	 * HTTP请求以\r\n\r\n结尾
	 * @param bb
	 * @return
	 */
	public static boolean isComplete(ByteBuffer bb) {
		ByteBuffer temp = bb.asReadOnlyBuffer();
		temp.flip();
		String data = requestCharset.decode(temp).toString();
		if(data.indexOf("\r\n\r\n")!= -1)	
			return true;
		return false;
	}

	/*
	 * 删除请求正文
	 * 仅支持GET和HEAD请求方式不处理HTTP请求的的正文
	 */
	private static ByteBuffer deleteContent(ByteBuffer bb){
		ByteBuffer temp = bb.asReadOnlyBuffer();
		String data = requestCharset.decode(temp).toString();
		if(data.indexOf("\r\n\r\n") != -1){
			data = data.substring(0, data.indexOf("\r\n\r\n") + 4);
			return requestCharset.encode(data);
		}
		return bb;
	}
	
//	private static Pattern requestPattern = Pattern.compile(
//			"\\A([A-Z]+)+([^]+)+HTTP/([0-9\\.]+)$" +
//					".*^Host:([^]+)$.*\r\n\r\n\\z"
//					, Pattern.MULTILINE | Pattern.DOTALL);
	
//	private static Pattern requestPattern = Pattern.compile(
//			"([A-Z]+)+[ ](.+)HTTP(.+)\\s*Host:(.+)\\s*$");
	
	public static Request parse(ByteBuffer bb) throws MalformedRequestException{
		bb = deleteContent(bb);	//删除请求正文
		CharBuffer cb = requestCharset.decode(bb);
		String data = cb.toString();
//		System.out.println(data);
		String[] datas = data.split("\\s");
		System.out.println(datas[0]);	// GET
		System.out.println(datas[1]);	// /test2.html
		System.out.println(datas[2]);	// HTTP/1.1
//		System.out.println(datas[4]);
		System.out.println(datas[5]);	// localhost:8080
		
//		Matcher m = requestPattern.matcher(cb.toString());
//		if(!m.matches()){
//			System.out.println("Not matches");
//			throw new MalformedRequestException();
//		}
		
		Action a;
		try{
//			a = Action.parse(m.group(1));
			a = Action.parse(datas[0]);
		}catch(IllegalArgumentException e){
			throw new MalformedRequestException();
		}
		URI u;
		try{
//			u = new URI("http://" + m.group(4) + m.group(2));
			u = new URI("http://" + datas[5] + datas[1]);
		}catch (URISyntaxException e) {
			throw new MalformedRequestException();
		}
//		return new Request(a, m.group(3), u);
		return new Request(a, datas[2], u);
	}

}
