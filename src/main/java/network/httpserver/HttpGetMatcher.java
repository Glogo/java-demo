package network.httpserver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpGetMatcher {

	private static Pattern requestPattern = Pattern.compile(
			"([A-Z]+)+[ ](.+)HTTP(.+)\\s*Host:(.+)\\s*$");

	public static void main(String[] args) {
		String ma = "GET /test2.html HTTP/1.1\r\nHost: localhost:8080";
		Matcher m = requestPattern.matcher(ma);
		System.out.println(m.matches());
		System.out.println(m.group(1));
	}
}
