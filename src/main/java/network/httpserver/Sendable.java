package network.httpserver;

import java.io.IOException;

public interface Sendable {

	//准备发送的正文
	public void prepare() throws IOException;
	
	//利用通道发送内容
	//所有内容发送完毕，返回false
	//有内容未发送，返回true
	//如果内容没准备好，返回IllegalStateException
	public boolean send(ChannelIO cio) throws IOException;
	
	//所有内容发送完毕，调用此方法，释放资源
	public void release() throws IOException;
	
} 
