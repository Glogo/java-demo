package network.httpserver;

import java.io.IOException;
import java.nio.ByteBuffer;

public class StringContent implements Content{

	private String content;
	
	private long length = -1;
	
	public StringContent(Exception e) {
		this.content = e.toString();
	}

	public StringContent(String string) {
		this.content = string;
	}

	@Override
	public void prepare() throws IOException {
		
	}

	@Override
	public boolean send(ChannelIO cio) throws IOException {
		ByteBuffer tmp = ByteBuffer.allocate(1024);
		tmp.put(content.getBytes());
		tmp.flip();
		return cio.write(tmp) > 0;
	}

	@Override
	public void release() throws IOException {
		
	}

	@Override
	public String type() {
		return content;
	}

	@Override
	public long length() {
		return content.getBytes().length;
	}

}
