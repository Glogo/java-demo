package network.httpserver;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;


public class AcceptHandler implements Handler{

	public void handle(SelectionKey key) throws IOException{
		ServerSocketChannel serverServerSocket = (ServerSocketChannel) key.channel();
		//在非阻塞模式下accept方法有可能返回null
		SocketChannel socketChannel = serverServerSocket.accept();
		if(socketChannel == null)	return;
		System.out.println("接受到客户连接，来自：" + socketChannel.socket().getInetAddress() + ":" + socketChannel.socket().getPort());
		
		ChannelIO cio = new ChannelIO(socketChannel, false);
		RequestHandler rh = new RequestHandler(cio);
		socketChannel.register(key.selector(), SelectionKey.OP_READ, rh);
	}
}
