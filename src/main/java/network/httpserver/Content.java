package network.httpserver;

public interface Content extends Sendable{
	//正文的类型
	String type();
	//正文的长度
	//正文没有准备之前，即还没用调用prepare之前，返回-1
	long length();
}
