package network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Simpel SocketServer
 * 在本地回环地址127.0.0.1上绑定8000端口
 * 与同包中的Client类对应
 */
public class Server {

    private static int Count = 1;

	private ServerSocket server;
	
	public Server() throws IOException {
		server = new ServerSocket(8000, 3, InetAddress.getByName("localhost"));
		System.out.println("服务器启动");
	}
	
	public void service(){
		while(true){
			Socket socket = null;
			try{
				socket = server.accept();
				System.out.println((Count++) +  " new connection accepted:" +
						socket.getInetAddress() + ":" + socket.getPort());
			}catch(IOException ioe){
				ioe.printStackTrace();
			}finally{
				try{
					if(socket != null)	socket.close();
				}catch(IOException ioe){	ioe.printStackTrace();	}
			}
		}
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Server s = new Server();
//		Thread.sleep(60000 * 10);
		s.service();
	}
}
