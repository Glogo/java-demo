package network;

import java.io.IOException;
import java.net.Socket;

/**
 * 与同包中的类Server对应
 */
public class Client {

	public static void main(String[] args) throws IOException, InterruptedException{
		Socket[] sockets = new Socket[100];
		
		for(int i = 0; i < 100; i++){
			sockets[i] = new Socket("localhost", 8000);
			System.out.println("第" + (i+1) + "次连接成功");
		}
		
		Thread.sleep(3000);
		for(int i = 0; i < 100; i++){
			sockets[i].close();
		}
	}
}
