package mass;

import java.util.StringTokenizer;

public class SplitDemo {

	public static void main(String[] args) {
		String orgStr;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < 1000; i++){
			sb.append(i);
			sb.append(";");
		}
		orgStr = sb.toString();
		long begin = System.currentTimeMillis();
		for(int i = 0; i < 10000; i++){
			orgStr.split(";");
		}
		System.out.println("spend:" + (System.currentTimeMillis() - begin));
		
		String orgStr2;
		StringBuilder sb2 = new StringBuilder();
		for(int i = 0; i < 1000; i++){
			sb2.append(i + 1);
			sb2.append(";");
		}
		orgStr2 = sb2.toString();
		StringTokenizer st = new StringTokenizer(orgStr2, ";");
		long begin2 = System.currentTimeMillis();
		for(int i = 0; i < 10000; i++){
			while(st.hasMoreTokens()){
				st.nextToken();
			}
			st = new StringTokenizer(orgStr2, ";");
		}
		System.out.println("spend2:" + (System.currentTimeMillis() - begin2));
		
		StringTokenizer st2 = new StringTokenizer("this is a java demo test");
		while(st2.hasMoreTokens()){
			System.out.println(st2.countTokens());
			System.out.println(st2.nextToken());
		}
	}
}
