package mass;

import java.util.ArrayList;
import java.util.List;

/**
 * @author glogo
 *
 */
public class SubStrStackOverflow {

	public static void main(String[] args) {
		List<String> handler = new ArrayList<>();
		for(int i=0; i < 10000; i++){
			HugStr h = new HugStr();
//			ImproveHugStr h = new ImproveHugStr();
			handler.add(h.getSubString(1, 5));
		}
	}
	
	static class HugStr{
		private String str = new String(new char[1000000]);
		public String getSubString(int begin, int end){
			return str.substring(begin, end);
		}
	}
	
	static class ImproveHugStr{
		private String str = new String(new char[1000000]);
		public String getSubString(int begin, int end){
			return new String(str.substring(begin, end));
		}
	}
	
}
