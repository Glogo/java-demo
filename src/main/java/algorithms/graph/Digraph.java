package algorithms.graph;

import java.util.ArrayList;

public class Digraph {

	private final int V;
	private int E;
	private ArrayList<Integer>[] adj;
	
	public Digraph(int V) {
		this.V = V;
		this.E = 0;
		adj = new ArrayList[V];
	}
	
	public int V(){return V;}
	public int E(){return E;}
	
	public void addEdge(int v, int w){
		adj[v].add(w);
		E++;
	}
	
	public Digraph reverse(){
		Digraph g = new Digraph(V);
		for(int v = 0; v < V; v++){
			for(int w : adj(v)){
				g.addEdge(w, v);
			}
		}
		return g;
	}
	
	public Iterable<Integer> adj(int v) {
		return adj[v];
	}
}
