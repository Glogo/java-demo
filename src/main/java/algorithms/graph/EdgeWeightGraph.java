package algorithms.graph;

import java.util.Set;
import java.util.TreeSet;

/*
 * 无向有权图
 */
public class EdgeWeightGraph {

	private final int V;
	private int E;
	private Set<Edge>[] adj;
	
	public EdgeWeightGraph(int V) {
		this.V = V;
		this.E = 0;
		adj = new TreeSet[V];
	}
	
	public int V(){
		return V;
	}
	
	public int E(){
		return E;
	}
	
	public Iterable<Edge> adj(int v){
		return adj[v];
	}
	
	public void addEdge(Edge e){
		int v = e.either();
		int w = e.other(v);
		adj[v].add(e);
		adj[w].add(e);
		E++;
	}
}
