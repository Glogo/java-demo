package algorithms.graph;


import algorithms.graph.DepthFirstOrder;
import algorithms.graph.Digraph;
import algorithms.graph.DirectedCycle;

public class Topological {

	private Iterable<Integer> order;
	
	public Topological(Digraph G) {
		DirectedCycle cycleFinder = new DirectedCycle(G);
		if(!cycleFinder.hasCycle()){
			DepthFirstOrder dfo = new DepthFirstOrder(G);
			order = dfo.reversePost();
		}
	}

	public Iterable<Integer> order(){
		return order;
	}
}
