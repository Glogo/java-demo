package algorithms.search;

import java.util.Iterator;
import java.util.ListIterator;

public class Queue<Item> implements Iterable<Item>{

	private Node first;
	private Node last;
	private int N;
	
	private class Node{
		Item item;
		Node next;
	}
	
	public boolean isEmpty(){
		return first == null;
	}
	
	public int size(){
		return N;
	}
	
	
	public void enqueue(Item item){
		Node oldlast = last;
		last = new Node();
		if(isEmpty())
		{
			first = last;
		}else{
			oldlast.next = last;
		}
		N++;
	}

	public Item dequeue(){
		Item item = first.item;
		first = first.next;
		if(isEmpty()){
			last = null;
		}
		N--;
		return item;
	}
	
	@Override
	public Iterator<Item> iterator() {
		return null;
	}

}
