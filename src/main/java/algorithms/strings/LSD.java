package algorithms.strings;

import java.util.Scanner;

/**
 * 低位优先的字符串排序
 *
 */
public class LSD {

    /**
     * @param a
     * @param w 字符串的长度均为w
     */
    public static void sort(String[] a, int w){
        int N = a.length;
        int R = 256;    //扩展ASCII可表示的字符,可参照Alphabet
        String[] aux = new String[N];

        for (int d = w-1; d >=0; d--) {
            int[] count = new int[R+1];
            //一趟遍历a，计算索引位置为d的字符的出现频率
            for (int i = 0; i < N; i++) {
                count[a[i].charAt(d) + 1]++;
            }
            //一趟遍历count，将频率转化为索引
            for (int r = 0; r < R; r++) {
                count[r+1] += count[r];
            }
            //将元素分类
            for (int i = 0;i < N; i++) {
                aux[count[a[i].charAt(d)]++] = a[i];
            }
            //将排序结果写回原数组
            System.arraycopy(aux, 0, a, 0, N);
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] a = scanner.nextLine().split("\\s");
        int N = a.length;

        // check that strings have fixed length
        int W = a[0].length();
        for (int i = 0; i < N; i++)
            assert a[i].length() == W : "Strings must have fixed length";

        // sort the strings
        sort(a, W);

        // print results
        for (int i = 0; i < N; i++)
            System.out.println(a[i]);
    }

}
