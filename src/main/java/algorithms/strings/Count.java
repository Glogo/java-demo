package algorithms.strings;

import java.util.Scanner;

/**
 * 利用Alphabet统计字符出现的频率
 */
public class Count {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("统计每个单词出现的次数");
//        Alphabet alpha = new Alphabet(args[0]);
        Alphabet alphabet = Alphabet.UPPERCASE;    //可以是其他的单词表

        int R = alphabet.R();
        int[] count = new int[R];
        String a = scanner.next();
        int N = a.length();
        for (int i = 0; i < N; i++)
            if (alphabet.contains(a.charAt(i)))
                count[alphabet.toIndex(a.charAt(i))]++;
        for (int c = 0; c < R; c++)
            System.out.println(alphabet.toChar(c) + " " + count[c]);

        scanner.close();
    }
}
