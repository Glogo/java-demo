package algorithms.sort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 希尔排序
 * @author Administrator
 *
 */
public class Shell {

	public static void sort(Comparable[] a){
		int N = a.length;
		int h = 1;
		while(h < N/3) 
			h = h* 3 + 1;
		while(h >= 1){
			for(int i = h; i < N; i++){
				for(int j = i; j >= h && less(a[j], a[j - h]); j -= h){
					exch(a, j, j - h);
				}
			}
			h= h / 3;
		}
	}
	
	private static boolean less(Comparable v, Comparable w){
		return v.compareTo(w) < 0;
	}
	
	private static void exch(Comparable[] a, int i, int j){
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	
	public static void show(Comparable[] a){
		for(int i = 0; i < a.length; i++){
			System.out.print(a[i] + " ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String s = br.readLine();
			String[] in = s.split(" ");	
			Shell.sort(in);
			Shell.show(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
