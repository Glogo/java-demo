package sets;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class TypesForSets {

	static <T> Set<T> fill(Set<T> set, Class<T> type){
		try{
			for(int i = 0; i < 10; i++){
				set.add(type.getConstructor(int.class).newInstance(i));
			}
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		return set;
	}
	
	static <T> void test(Set<T> set, Class<T> type){
		fill(set, type);
		fill(set, type);//加重复的值
		fill(set, type);
		System.out.println(set);
	}
	
	public static void main(String[] args) {
		test(new HashSet<HashType>(), HashType.class);
		test(new LinkedHashSet<HashType>(), HashType.class);
		test(new TreeSet<TreeType>(), TreeType.class);
//		test(new HashSet<SetType>(), TreeType.class); //编译错误
		test(new HashSet<TreeType>(), TreeType.class);	//hashset判断两个对象相等除了equals还要hashCode相等
		test(new LinkedHashSet<SetType>(), SetType.class);
		test(new LinkedHashSet<TreeType>(), TreeType.class);
		
		//如果尝试在TreeSet中添加没有实现Comparable接口的类型，直接报出异常
		try{
			test(new TreeSet<SetType>(), SetType.class);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		try{
			test(new TreeSet<HashType>(), HashType.class);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
}
