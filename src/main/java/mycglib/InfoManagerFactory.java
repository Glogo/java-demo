package mycglib;

import net.sf.cglib.proxy.Enhancer;

public class InfoManagerFactory {

	private static InfoManager manger = new InfoManager();
   
    public static InfoManager getInstance() {
        return manger;
    }
    
    
    public static InfoManager getAuthInstance(AuthProxy auth) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(InfoManager.class);
        enhancer.setCallback(auth);
        return (InfoManager) enhancer.create();
    }
    
}
