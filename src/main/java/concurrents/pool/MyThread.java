package concurrents.pool;

class MyThread implements Runnable{
	protected String name;
	public MyThread(String name) {
		this.name = name;
	}
	@Override
	public void run() {
		try{
			Thread.sleep(100);
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}
	}
}
