package concurrents.pool;

public class MainTest {

	public static void main(String[] args) {
		long currentTime = System.currentTimeMillis();
		
		for(int i = 0; i < 170000; i++){
			new Thread(new MyThread("testThreadPool" + Integer.toString(i))).start();
//			System.out.println(i);
		}
		
		System.out.println("time cost = " + (System.currentTimeMillis() - currentTime));
		currentTime = System.currentTimeMillis();
		
		for(int i = 0; i < 170000; i++){
			MyThreadPoolDemo2.getInstance().start(new MyThread("testThreadPool" + Integer.toString(i)));
//			System.out.println(i);
		}
		
		System.out.println("time cost = " + (System.currentTimeMillis() - currentTime));
	}
	
}
