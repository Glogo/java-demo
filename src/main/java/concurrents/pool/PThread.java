package concurrents.pool;

public class PThread extends Thread{

	private MyThreadPoolDemo2 pool;
	private Runnable target;
	private boolean isShutDown = false;
	private boolean isIdle = false;
	
	public PThread(Runnable target, String name, MyThreadPoolDemo2 pool) {
		super(name);
		this.pool = pool;
		this.target = target;
	}

	@Override
	public void run() {
		while(isShutDown){
			isIdle = false;
			if(target != null){
				target.run();
			}
			//任务结束
			isIdle = true;
			try{
				pool.repool(this);
				synchronized(this){
					wait();
				}
			}catch(InterruptedException ie){}
			isIdle = false;
		}
	}

	public Runnable getTarget(){
		return target;
	}
	
	public boolean isIdle(){
		return isIdle;
	}
	
	public void shutDown() {
		isShutDown = true;
		notifyAll();
	}

	public void setTarget(Runnable newTarget) {
		target = newTarget;
		notifyAll();
	}

}
