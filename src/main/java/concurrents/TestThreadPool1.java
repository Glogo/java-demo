package concurrents;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestThreadPool1 {
	
	public static void main(String[] args) {
		
		ExecutorService exec = Executors.newFixedThreadPool(2);
		for(int i = 0; i < 1000; i++){
			Runnable run = new Runnable() {
				@Override
				public void run() {
					long time = (long)(Math.random() * 1000);
					System.out.println("Thread-1:休眠" + time + "ms");
					try{
						Thread.sleep(time);
					}catch(InterruptedException ie){
						ie.printStackTrace();
					}
				}
			};
			exec.execute(run);
		}
		
		//顺序执行
		for(int i = 0; i < 1000; i++){
			Runnable run = new Runnable() {
				@Override
				public void run() {
					long time = (long)(Math.random() * 1000);
					System.out.println("Thread-2:休眠" + time + "ms");
					try{
						Thread.sleep(time);
					}catch(InterruptedException ie){
						ie.printStackTrace();
					}
				}
			};
			exec.execute(run);
		}
		
		
		exec.shutdown();
	}
}
