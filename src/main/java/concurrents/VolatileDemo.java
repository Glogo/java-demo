package concurrents;

/**
 * Volatile关键字修饰的成员变量，其实是强迫线程每次都从共享内存中重读该变量的值
 * 而且当变量发生变化时，强迫线程将变化值写回到共享内存
 */
public class VolatileDemo {

	volatile boolean isExit = false;
	
	public void tryExit(){
		System.out.println("tryExit:" + isExit);
		/*
		 * 这一句话有可能成立的，因为isExit声明为volatile，它的修改对所有线程均可见
		 * 在获取到了左边的值后，读取右边的值之前
		 * swap很有可能已经修改了isExit的值
		 */
		if(isExit == ! isExit) {
            System.exit(0);
        }
	}
	
	public void swap(){
		System.out.println("swap:" + isExit);
		this.isExit = !isExit;
	}
	
	public static void main(String[] args){
		final VolatileDemo volObj = new VolatileDemo();
		Thread mainThread = new Thread(){
			@Override
			public void run() {
				System.out.println("main Thread start");
				while(true){
					volObj.tryExit();
				}
			}
		};
		mainThread.start();
		Thread swapThread = new Thread(){
			@Override
			public void run() {
				System.out.println("swap Thread start");
				while(true){
					volObj.swap();
				}
			}
		};
		swapThread.start();
	}
	
}
