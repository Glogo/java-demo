package concurrents;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 重入锁（同一个线程在获得了锁但没有释放之前，可以再次获得锁）
 * 可重入锁指在同一个线程中，可以重入的锁。
 * 当然，当这个线程获得锁后，其他线程将等待这个锁被释放后，才可以获得这个锁。
 * 1.公平锁 所谓公平的锁，是指，各个希望获得所得线程获得锁的顺序是按到达的顺序获得
 * 2.非公平锁 自由竞争获得锁。
 * @author glogo
 *
 */
public class ReentrantLockTest {

	ReentrantLock lock = new ReentrantLock();	//非公平锁
	
	private Runnable createTask(){
		return new Runnable(){
			@Override
			public void run() {
				while(true){
					try{
						if(lock.tryLock(500, TimeUnit.MILLISECONDS)){
							try{
								System.out.println("locked:" + Thread.currentThread().getName());
								Thread.sleep(1000);
							}finally{
								lock.unlock();
								System.out.println("unlocked:" + Thread.currentThread().getName());
							}
							break;
						}else{
							System.out.println("unable to lock " + Thread.currentThread().getName());
						}
					}catch(InterruptedException ie){
						System.out.println(Thread.currentThread().getName() + " is Interrupted.");
					}
				}
			}
		};
	}
	
	public void test(){
		Thread t1 = new Thread(createTask(), "first-1");
		Thread t2 = new Thread(createTask(), "second-2");
		t1.start();
		t2.start();
		t2.interrupt();	//thread-2线程处理完毕中断之后会继续尝试获得锁
	}
	
	public static void main(String[] args) {
		new ReentrantLockTest().test();
	}
	
	
	
}
