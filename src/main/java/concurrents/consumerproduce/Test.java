package concurrents.consumerproduce;

public class Test {

	public static void main(String[] args) {
		
		ProductList ps = new ProductList();
		Producer px = new Producer(ps, "生产者X");
		Producer py = new Producer(ps, "生产者Y");
		Consumer ca = new Consumer(ps, "消费者a");
		Consumer cb = new Consumer(ps, "消费者b");
		Consumer cc = new Consumer(ps, "消费者c");
		new Thread(px).start();
		new Thread(py).start();
		new Thread(ca).start();
		new Thread(cb).start();
		new Thread(cc).start();
		
	}
}
