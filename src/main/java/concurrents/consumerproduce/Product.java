package concurrents.consumerproduce;

public class Product {

	int id;
	private String producedBy = "n/a";
	private String consumedBy = "n/a";
	
	public Product(String productBy) {
		this.producedBy = productBy;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProducedBy() {
		return producedBy;
	}
	public void setProducedBy(String producedBy) {
		this.producedBy = producedBy;
	}
	public String getConsumedBy() {
		return consumedBy;
	}
	public void setConsumedBy(String consumedBy) {
		this.consumedBy = consumedBy;
	}
	
	@Override
	public String toString() {
		return "产品，生产者 = " + producedBy + ", 消费者 = " + consumedBy;
	}
	
	
}
