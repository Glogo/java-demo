package concurrents.consumerproduce;

public class Producer implements Runnable{

	String name;
	ProductList ps;
	public Producer(ProductList ps, String name) {
		this.ps = ps;
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true){
			Product product = new Product(name);
			ps.push(product);
			try{
				Thread.sleep((int)(Math.random() * 2000));
			}catch(InterruptedException ie){
				ie.printStackTrace();
			}
		}
		
	}

}
