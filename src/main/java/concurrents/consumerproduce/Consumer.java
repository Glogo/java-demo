package concurrents.consumerproduce;

public class Consumer implements Runnable{

	String name;
	ProductList ps;
	
	public Consumer(ProductList ps, String name) {
		this.ps = ps;
		this.name = name;
	}
	
	@Override
	public void run() {
		while(true){
			ps.pop(name);
			try{
				Thread.sleep((int)(Math.random() * 5000));
			}catch(InterruptedException ie){
				ie.printStackTrace();
			}
		}
		
	}

}
