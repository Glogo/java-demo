package concurrents.future;


public class Client {

	public Data request(final String queryStr){
		final FutureData future = new FutureData();
		new Thread(){
			public void run() {
				RealData readata = new RealData(queryStr);
				future.setRealdata(readata);
			}
		}.start();
		return future;
	}
}
