package concurrents.future;

public class Main {

	public static void main(String[] args) {
		Client client = new Client();
		Data data = client.request("name;");
		System.out.println("请求完毕");
		try{
			System.out.println("正在处理");
			Thread.sleep(2000);
		}catch(InterruptedException ie){}
		System.out.println("数据 = " + data.getResult());
	}
}
