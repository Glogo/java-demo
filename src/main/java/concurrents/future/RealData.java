package concurrents.future;

public class RealData implements Data{

	protected final String result;
	
	public RealData(String para) {
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < 10; i++){
			sb.append(para);
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){}
		}
		result = sb.toString();
	}
	
	
	public String getResult() {
		return result;
	}
}
