package concurrents.future;

public interface Data {

	public String getResult();
}
