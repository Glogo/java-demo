package concurrents;

public class VolatileMyThread extends Thread {

	private volatile boolean stop = false;	//不论哪种模式下都可以正常工作
//	private boolean stop = false;	//-server模式下工作，不用volatile就会出问题
	
	public void stopMe(){
		this.stop = true;
	}
	
	@Override
	public void run() {
		int i = 0;
		while(!stop){
			i++;
		}
		System.out.println("Stop Thread.");
	}
	
	public static void main(String[] args) {
		VolatileMyThread t = new VolatileMyThread();
		t.start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t.stopMe();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
