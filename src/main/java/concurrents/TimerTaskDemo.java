package concurrents;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * java.util.Timer是一个工具类，可以用于安排一个线程在未来的某个特定时间执行。
 * Timer类可以用安排一次性任务或者周期任务。
 */
public class TimerTaskDemo extends TimerTask{

	@Override
	public void run() {
		System.out.println(new Date());
	}

	public static void main(String[] args) {
		Timer timer = new Timer();	    //需要Timer启动TimeTask
		TimerTaskDemo task = new TimerTaskDemo();
		timer.schedule(task, 1000, 1000);
	}
}
