package concurrents;

//工作线程，调用TestServer.testRun
public class WorkThread implements Runnable {

		private ReentrantLockTest2 tester = null;

		public WorkThread(ReentrantLockTest2 testLock) {
			this.tester = testLock;
		}
		public void run() {
			// 循环调用，尝试加锁，并对共享数据+1，然后显示出来
			while (true) {
				try {
					// 调用tester.testRun()
					tester.testRun();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
