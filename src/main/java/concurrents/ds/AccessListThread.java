package concurrents.ds;

import java.util.List;
import java.util.Random;

public class AccessListThread implements Runnable{

	protected String name;
	private List<Integer> list;
	private Random rand = new Random();
	
	public AccessListThread(String name, List<Integer> list) {
		this.name = name;
		this.list = list;
	}
	
	@Override
	public void run() {
		try{
			for(int i = 0; i < 500; i++){
				getList(rand.nextInt(100));
			}
			Thread.sleep(100);
		}catch(InterruptedException ie){
			ie.printStackTrace();
		}
	}

	
	public Object getList(int index){
		return list.get(index);
	}
}
