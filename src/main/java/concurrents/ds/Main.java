package concurrents.ds;

import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Main {

	private static final int TASK_COUNT = 4000;
	
	private static List<Integer> concurrentList;
//	private List<Integer> concurrentList = new Vector<Integer>();
	
	public Main() {
		concurrentList = new CopyOnWriteArrayList<Integer>();
//		concurrentList = new Vector<Integer>();
		for(int i = 0; i < 100; i++)
			concurrentList.add(i);
	}
	
	
	public static void main(String[] args) {
		new Main();
		CounterPoolExecutor exe = new CounterPoolExecutor(
				2000, 2000, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
		long startTime = System.currentTimeMillis();
		exe.startTime = startTime;
		exe.funcName = "testCopyOnWriteList";
		for(int i = 0; i < TASK_COUNT; i++){
			exe.submit(new AccessListThread(Integer.toString(i), concurrentList));
		}
		exe.shutdown();
	}
}
