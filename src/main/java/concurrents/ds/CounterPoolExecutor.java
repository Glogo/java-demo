package concurrents.ds;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterPoolExecutor extends ThreadPoolExecutor{

	private AtomicInteger count = new AtomicInteger(0);
	public long startTime = 0;
	public String funcName = "";
	
	public CounterPoolExecutor(int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	//回调方法，每次一个任务执行完毕就会调用
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		int l = count.addAndGet(1);
		if(l == 4000){
			System.out.println(funcName + "spend time:" 
					+ (System.currentTimeMillis() - startTime));
		}
	}
	
}
