package concurrents.master_worker;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Master {

	//Task queue
	protected Queue<Object> workQueue = new ConcurrentLinkedQueue<Object>();
	//Worker thread queue
	protected Map<String, Thread> threadMap = new HashMap<>();
	//Subtask result set
	protected Map<String, Object> resultMap = new ConcurrentHashMap<String, Object>();
	
	//is all of subtask complete
	public boolean isComplete(){
		for(Map.Entry<String, Thread> entry : threadMap.entrySet() ){
			if(entry.getValue().getState() != Thread.State.TERMINATED){
				return false;
			}
		}
		return true;
	}
	
	public Master(Worker worker, int countWorker) {
		worker.setWorkQueue(workQueue);
		worker.setResultMap(resultMap);
		for(int i = 0; i < countWorker; i++){
			threadMap.put(Integer.toString(i), new Thread(worker, Integer.toString(i)));
		}
	}

	//submit a task
	public void submit(Object job){
		workQueue.add(job);
	}
	
	//return the task set
	public Map<String, Object> getResultMap(){
		return resultMap;
	}
	
	//start all the Worker's thread
	public void execute(){
		for(Map.Entry<String, Thread> entry : threadMap.entrySet()){
			entry.getValue().start();
		}
	}
	
}
