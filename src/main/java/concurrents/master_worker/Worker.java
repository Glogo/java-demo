package concurrents.master_worker;

import java.util.Map;
import java.util.Queue;

public class Worker implements Runnable{

	protected Queue<Object> workQueue;
	
	protected Map<String, Object> resultMap;
	
	public void setWorkQueue(Queue<Object> workQueue) {
		this.workQueue = workQueue;
	}

	public void setResultMap(Map<String, Object> resultMap) {
		this.resultMap = resultMap;
	}
	
	//应用程序通过重载次方法实现应用层逻辑
	public Object handler(Object input){
		return input;
	}

	@Override
	public void run() {
		while(true){
			Object input = workQueue.poll();
			if(input == null)	break;
			Object re = handler(input);
			resultMap.put(Integer.toString(input.hashCode()), re);
		}
	}

}
