package concurrents.master_worker;

public class PlusWorker extends Worker{

	@Override
	public Object handler(Object input) {
		Integer i = (Integer)input;
		return i*i*i;
	}
}
