package fun;

/*
 * 垃圾回收器实现方式：
 * 1.并发方式
 * 2.暂停方式
 * 
 */
public class GCTest {

	public static void main(String[] args) {
		System.gc();		//这个会建议垃圾回收器立即运行
	}
}
