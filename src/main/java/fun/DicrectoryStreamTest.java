package fun;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DicrectoryStreamTest {

	public static void main(String[] args) {
		Path path = Paths.get("/home/glogo/workspace/learn/src/main/java/");	//当前目录
		DirectoryStream<Path> stream = null;
		try{
			//DirectoryStream只能遍历当前目录下的子目录或文件
			stream = Files.newDirectoryStream(path);
//			System.out.println(stream);
			for(Path entry : stream){
				System.out.println(entry.getFileName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
