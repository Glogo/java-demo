package fun;

import java.lang.ref.SoftReference;

/*
 * 引用类型
 */
public class SoftRefTest {

	public static void main(String[] args) {
		//创建一个强引用
		Object obj = new Object();
		//创建一个软引用
		SoftReference<Object> ref = new SoftReference<Object>(obj);
		obj = null;	//清除强引用
		//以下这种做法是错误的:没有强引用指向待引用的对象，垃圾回收器可能随时进行内存的回收工作
		//SoftReference<Object> ref2 = new SoftReference<Object>(new Object());
		ref.get();	//返回该对象
		ref.clear();	//清除这个对象的引用，垃圾回收器就会在合适的时候回收这个对象的空间
		
	}
}
