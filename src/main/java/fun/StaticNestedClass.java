package fun;

import java.lang.reflect.Constructor;

public class StaticNestedClass {

	public StaticNestedClass(String name) {
	}
	
	class NestedClass{
		public NestedClass(int count) {
		}
	}
	
	public void userNestedClassConstructor() throws Exception{
		Constructor<StaticNestedClass> sncc = StaticNestedClass.class.getDeclaredConstructor(String.class);
		sncc.newInstance("Alex");
		Constructor<NestedClass> ncc = NestedClass.class.getDeclaredConstructor(int.class);
	}
	
}
