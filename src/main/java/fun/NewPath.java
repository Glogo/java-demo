package fun;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NewPath {

	public static void main(String[] args) {
		Path path1 = Paths.get("folder1", "sub1");
		Path path2 = Paths.get("folder2", "sub2");
//		System.out.println(path2);
		System.out.println(path1.resolve(path2));
//		System.out.println(path1);
		System.out.println(path1.resolveSibling(path2));
		System.out.println(path1.relativize(path2));
		System.out.println(path1.subpath(0, 1));
		System.out.println(path1.startsWith(path2));
		System.out.println(path1.endsWith(path2));
	}
}
