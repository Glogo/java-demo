package fun;

import java.nio.ByteBuffer;

public class ByteBufSliceTest {

	public static void main(String[] args) {
		ByteBuffer buffer = ByteBuffer.allocate(10);
		for(int i = 0; i < buffer.capacity(); i++){
			buffer.put((byte)i);
		}
		buffer.position(3);
		buffer.limit(7);
		//创建一个片段，片段是原缓冲区的子缓冲区，不过它们共享的是同一个底层数据数组
		ByteBuffer slice = buffer.slice();
//		System.out.println(slice.capacity());
//		System.out.println(slice.get(2));
		for(int j = 0; j < slice.capacity(); j++){
			byte b = slice.get(j);
			b *= 11;
			slice.put(j, b);
		}
	    buffer.position( 0 );  
	    buffer.limit( buffer.capacity() );  
	     while (buffer.remaining()>0) {  
	         System.out.println( buffer.get() );  
	    } 
		
	}
}
