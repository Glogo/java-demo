package fun;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

/**
 * 目录监视器
 * @author glogo
 *
 */
public class FileWatch {

	public static void main(String[] args) throws IOException, InterruptedException {
		WatchService service = FileSystems.getDefault().newWatchService();
		Path path = Paths.get("").toAbsolutePath();
		System.out.println(path);
		path.register(service, StandardWatchEventKinds.ENTRY_CREATE);	//为这个路径注册一个监视器
		while(true){
			WatchKey key = service.take();
			for(WatchEvent<?> event : key.pollEvents()){
				Path createdPath = (Path) event.context();	//与目录内容相关的上下文
				createdPath = path.resolve(createdPath);
				long size = Files.size(createdPath);
				System.out.println(createdPath + "===>" + size);
			}
		}
		
	}
}
