package fun;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class QiuShiBaiKe {

	private static int currentPage;
	private static String split = "++++++++++++++++++++++++";

	public static void main(String[] args) throws IOException {
		System.out.println("Please input the page:");
		BufferedReader buff = new BufferedReader(new InputStreamReader(
				System.in));
		int index = 0;
		String temp;
		while (true) {
			temp = buff.readLine();
			index = toInt(temp);
			if (index == 0) {
				System.out.println("请输入页码：" + (++currentPage) + "ҳ\n" + split);
			} else {
				currentPage = index;
				System.out.println("当前页：" + currentPage + "ҳ\n" + split);
			}
			System.out.println(getUrl(currentPage));
		}
	}

	private static int toInt(String dd) {
		try {
			return Integer.parseInt(dd);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private static String getUrl(int page) {
		StringBuffer sb = new StringBuffer();
		try {
			String url = "http://www.qiushibaike.com/month/page/" + page
					+ "?s=4595690&slow";

			Document doc = Jsoup
					.connect(url)
					.timeout(60000)
					.userAgent(
							"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:23.0) Gecko/20100101 Firefox/23.0")
					.get();
			Elements newsHeadlines = doc.select("div.content");
			for (Element e : newsHeadlines) {
				sb.append(changeLine(e.text()));
				sb.append("\n").append(split).append("\n");
			}
		} catch (Exception e) {
			e.printStackTrace(); 
		}
		return sb.toString();
	}

	private static String changeLine(String line) {
		StringBuffer sb = new StringBuffer(line);
		for (int i = 0; i < line.length() / 60; i++) {
			sb.insert((i + 1) * 60, "\n");
		}
		return sb.toString();
	}

}
