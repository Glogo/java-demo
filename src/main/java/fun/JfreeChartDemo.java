package fun;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class JfreeChartDemo {

	public static void main(String[] args) {
		DefaultPieDataset dpd = new DefaultPieDataset();
		dpd.setValue("1-25", 25);
		dpd.setValue("25-50", 25);
		dpd.setValue("50-75", 45);
		dpd.setValue("75-100", 10);

		JFreeChart chart = ChartFactory.createPieChart3D("Բ��ͼ", dpd, true, true, false);
		ChartFrame chartFrame = new ChartFrame("OLD", chart);
		chartFrame.setLocation(300, 300);
		chartFrame.pack();
		chartFrame.setVisible(true);
		chartFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
