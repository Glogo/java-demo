package fun;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * @author glogo
 *
 */
public class WeakRefTest {

	private class Book{}
	private class User{}
	
//	private Map<Book, Set<User>> books = new HashMap<Book, Set<User>>();
	private Map<Book, Set<User>> books = new WeakHashMap<Book, Set<User>>();
	
	public void borrowBook(Book book, User user){
		Set<User> users = null;
		if(books.containsKey(book)){
			users = books.get(user);
		}else{
			users = new HashSet<User>();
			books.put(book, users);
		}
		users.add(user);
	}
	
	
	
}
