package fun;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

/**
 * Java NIO方式遍历文件夹
 * @author glogo
 *
 */
public class FileVisitorTest {

	static class MyFileVisitor implements FileVisitor{

		@Override
		public FileVisitResult preVisitDirectory(Object dir,
				BasicFileAttributes attrs) throws IOException {
			//访问文件夹之前调用  
			System.out.println("访问文件夹之前");
            System.out.println(dir);  
            return FileVisitResult.CONTINUE;  
		}

		@Override
		public FileVisitResult visitFile(Object file, BasicFileAttributes attrs)
				throws IOException { 
			// 访问文件调用  
			System.out.print("访问文件: ");
            System.out.println(file);  
            System.out.println("创建时间：" + attrs.creationTime() +
            		" 最后修改时间：" + attrs.lastModifiedTime());
            return FileVisitResult.CONTINUE; 
		}

		@Override
		public FileVisitResult visitFileFailed(Object file, IOException exc)
				throws IOException {
			// 访问文件失败时调用  
            return FileVisitResult.CONTINUE;  
		}

		@Override
		public FileVisitResult postVisitDirectory(Object dir, IOException exc)
				throws IOException {
			 // 访问文件夹之后调用  
			System.out.println("访问文件夹之后");
            return FileVisitResult.CONTINUE;  
		}
		
	}
	
	public static void main(String[] args) {
		try {
			Files.walkFileTree(Paths.get("/home/glogo/workspace/learn/src/main/java/fun/"), new MyFileVisitor());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
