package fun;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

//ByteBuffer的字节存放次序
/**
 * ByteBuffer是以高位优先的形式存储数据的，并且数据在网络传送时也常常使用高位优先的形式
 * 例如：
 * 高位优先：00000000 01100001 -- 97
 * 低位优先：01100001 00000000 -- 24832
 * @author glogo
 *
 */
public class Endians {

	public static void main(String[] args) {
		
		ByteBuffer bb = ByteBuffer.wrap(new byte[12]);
		bb.asCharBuffer().put("abcdef");
		System.out.println(Arrays.toString(bb.array()));	//还是ByteBuffer的输出
		bb.rewind();
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.asCharBuffer().put("abcdef");
		System.out.println(Arrays.toString(bb.array()));
		bb.rewind();
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.asCharBuffer().put("abcdef");
		System.out.println(Arrays.toString(bb.array()));
	}
	
}
