package fun;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class JSoupDemo {

	
	public static void main(String[] args) {
//		StringBuffer sb = new StringBuffer();
		String html = "<html><head><title> jsoup学习 </title></head>"
				  + "<body><p id='jsoup'>  jsoup  </p>" +
				  "<p id='dom4j'>  dom4j  </p>" +
				  "</body></html>"; 
		try{
			Document doc = Jsoup.parse(html);
			Element hElement = doc.body();
			System.out.println(hElement.nodeName());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
