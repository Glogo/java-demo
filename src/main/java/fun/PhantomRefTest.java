package fun;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;

public class PhantomRefTest {

	private static class ReferencedObject{
		@Override
		protected void finalize() throws Throwable {
			System.out.println("finalize����������");
			super.finalize();
		}
	}
	
	public void phantomReferencedQueue(){
		ReferenceQueue<ReferencedObject> queue = new ReferenceQueue<>();
		ReferencedObject obj = new ReferencedObject();
		/*
		 * ������Ŀɴ�״̬����仯ʱ��������ϣ��õ�֪ͨ�����ָ��ö�������������ö��й�������
		 * �������ö��л�ȡ�����ö���֮����ζ�Ų������ٻ�ȡ����ָ��ľ��������
		 * ���仰˵�������󱻻��գ��������ý����������ö���
		 * ���������ú������ã��ڱ��������֮ǰ�����ǵ����ù�ϵ�Ѿ������
		 * ������finalize����������֮���������òŻᱻ��ӵ�������
		 */
		PhantomReference<ReferencedObject> phantomRef = 
				new PhantomReference<ReferencedObject>(obj, queue);
		obj = null;
		Reference<? extends ReferencedObject> ref = null;
		int i = 1;
		while((ref = queue.poll()) == null){
			System.out.println(i++);
			System.gc();
		}
//		phantomRef.get();//������Ϊnull,������ͨ���������õõ�������κβ���
//		��ͱ�����finallize�������ܻ���ֵĶ��󸴻�����
//		����������Ϊһ��֪ͨ���ƶ���ڣ������ڵõ�֪֮ͨ������뵱ǰ������ص����?��
		phantomRef.clear();	//���������ڱ���ӵ�����֮ǰ�������ù�ϵ���ᱻ��������Ҫ����clear��������ʾ�����
		System.out.println(ref == phantomRef);
		System.out.println("�������ñ����");
	}
	
	public static void main(String[] args) {
		new PhantomRefTest().phantomReferencedQueue();
	}
	
}
