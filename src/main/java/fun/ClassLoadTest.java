package fun;

public class ClassLoadTest {

	public static void main(String[] args) throws Exception{
		ClassLoader cl = ClassLoadTest.class.getClassLoader();
		ClassLoader scl= ClassLoader.getSystemClassLoader();	//得到系统类加载器
		ClassLoader acl= scl.getParent();		//得到扩展类加载器
		Class<?> clazz = cl.loadClass("java.lang.String");
		String str     = (String) clazz.newInstance();	//这跟直接使用String.class是一样的
		System.out.println(str.length());
		System.out.println(cl);
		System.out.println(scl);
		System.out.println(acl);
	}
}
