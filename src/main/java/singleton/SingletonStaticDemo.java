package singleton;

/**
 * 单例模式的一种写法
 */
public class SingletonStaticDemo {

	private SingletonStaticDemo(){
		System.out.println("StaticSingleton is create");
	}
	
	private static class SingletonHolder{
		private static SingletonStaticDemo instance = new SingletonStaticDemo();
	}
	
	public static SingletonStaticDemo getInstance(){
		return SingletonHolder.instance;
	}
	
	public static void say(){
		System.out.println("aa");
	}
	
}
