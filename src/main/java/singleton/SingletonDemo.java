package singleton;

public class SingletonDemo extends Thread{

	long begintime = System.currentTimeMillis();
	
	@Override
	public void run() {
		for(int i = 0; i < 100000; i++)
		{
			Singleton.getInstance();
//			LazySingleton.getInstance();
		}
		System.out.println("speed:" + (System.currentTimeMillis() - begintime));
	}
	
	public static void main(String[] args) {
//		new SingletonDemo().start();
		SingletonStaticDemo.say();
	}
}
