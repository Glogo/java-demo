package singleton;

public class SingletonLazy {

	private SingletonLazy(){
		System.out.println("LazySingleton is create");
	}
	
	private static SingletonLazy instance = null;

	public static synchronized SingletonLazy getInstance(){
		if(instance == null)
			instance = new SingletonLazy();
		return instance;
	}
	
}
