package singleton;

/**
 * 单例模式的几种写法
 */
public class Singleton {

	private Singleton(){
		System.out.println("Singleton is create");
	}
	
	private static Singleton instance = new Singleton();
	
	public static Singleton getInstance(){
		return instance;
	}
	
}
