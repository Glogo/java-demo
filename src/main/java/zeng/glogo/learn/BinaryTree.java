package zeng.glogo.learn;

public class BinaryTree {
	
	private TreeNode root;	//根节点
	
	protected TreeNode getRoot() {
		return root;
	}

	protected void setRoot(TreeNode root) {
		this.root = root;
	}

	public BinaryTree() {
	}
	
	public BinaryTree(int nodeValue){
		root = new TreeNode();
		TreeBean nodeBean = new TreeBean();
		nodeBean.setNodeValue(nodeValue);
		root.setData(nodeBean);
	}
	
	/**
	 * 销毁树
	 */
	public static BinaryTree destroy(){
		return null;
	}
	
	//插入结点
	public void insert(TreeNode root, TreeNode node){
		if(root == null)
			root = node;
		else{
			//根节点的左边
			if(node.getData().getNodeValue() < root.getData().getNodeValue())
			{
				if(root.getLeftNode() == null)	//根节点左边为空
					root.setLeftNode(node);
				else
					insert(root.getLeftNode(), node);	//递归插入
			}else{
				if(root.getRightNode() == null)
					root.setRightNode(node);
				else
					insert(root.getRightNode(), node);	//递归插入
			}
		}
	}
	
	//前序遍历
	public String FIterator(TreeNode root){
		StringBuilder str = new StringBuilder();
		if(root == null)	return str.toString();
		else{
			if(root.getData() != null)
				str.append(root.getData().getNodeValue());
			else
				str.append("null");
			
			str.append(FIterator(root.getLeftNode()));
			str.append(FIterator(root.getRightNode()));
		}
		return str.toString();
	}
	
	//后续遍历
	public String AIterator(TreeNode root){
		StringBuilder str = new StringBuilder();
		//有了这一句就不用再判断下面的if(root.getLeftNode() != null)和
		if(root == null)	return str.toString();
		else{
			str.append(AIterator(root.getLeftNode()));
			str.append(AIterator(root.getRightNode()));
			
			if(root.getData() != null)
				str.append(root.getData().getNodeValue());
			else
				str.append("null");
		}
		
		return str.toString();
	}
	
	
}



