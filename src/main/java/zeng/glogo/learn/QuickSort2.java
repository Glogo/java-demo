package zeng.glogo.learn;

import java.util.Scanner;

public class QuickSort2 {

	static int n=0;
	static int[] array;
	
	private void Swap(int i, int j){
		int c=array[i];
		array[i]=array[j];
		array[j]=c;
	}
	
	public void QuickSort(){
		QuickSort(0, n-1);
	}
	private void QuickSort(int left, int right){
		if(left<right){
			int j = Partition(left, right);
			QuickSort(left, j - 1);
			QuickSort(j + 1, right);
		}
	}
	
	private int Partition(int left, int right){
		int i=left,j=right+1;
		do{
			do
				i++;
			while(array[i]<array[left]);
			
			do
				j--;
			while(array[j]>array[left]);
			
			if(i<j)
				Swap(i, j);
		}while(i<j);
		//最后比较结束，交换left和j对应的数组的值
		Swap(left, j);
		return j;	//返回j下标
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		n=scanner.nextInt();
		array=new int[n];
		scanner.close();
	}

}
