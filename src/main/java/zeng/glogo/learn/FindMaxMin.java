package zeng.glogo.learn;

public class FindMaxMin {

	/** 分治法求最大值最小值 */
	public void MaxMin(int i, int j, int[] maxmin, int[] a){
		int[] maxmin1 = new int[2];
		if(i == j) 							//表中只有一个元素
			maxmin[0] = maxmin[1] = a[i];
		else if(i == j-1){ 					//表中只有两个元素
			if(a[i] < a[j]){
				maxmin[0] = a[j];
				maxmin[1] = a[i];
			}else{
				maxmin[0] = a[i];
				maxmin[1] = a[j];
			}
		}else{
			int m = (i + j) / 2;
			MaxMin(i, m, maxmin, a);
			MaxMin(m+1, j, maxmin, a);
			if(maxmin[0] < maxmin1[0])
				maxmin[0] = maxmin[0];
			if(maxmin[1] > maxmin1[1])
				maxmin[1] = maxmin1[1];
		}
	}
	
	public static void main(String[] args) {
		
	}

}
