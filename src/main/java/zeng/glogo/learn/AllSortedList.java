package zeng.glogo.learn;

public class AllSortedList {

	public void Perm(int[] a, int k, int n){
		if(k == n-1){
			for(int i = 0; i < n; i++)
				System.out.print(a[i]+" ");
			System.out.println();
		}
		else{
			for(int i = k; i < n; i++){
				int t = a[k];
				a[k] = a[i];
				a[i] = t;
				Perm(a, k+1, n);
				t = a[k];
				a[k] = a[i];
				a[i] = t;
			}
		}
	}
	
	public static void main(String[] args) {
		
		AllSortedList all = new AllSortedList();
		int[] a = {1,2,3,4};
		all.Perm(a, 0, a.length);
	}
}
