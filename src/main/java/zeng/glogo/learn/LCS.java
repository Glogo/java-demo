package zeng.glogo.learn;



public class LCS {

	static int count = 0;
	
	 public static int[][] getLength(String[] x, String[] y)  
	    {  
	        int[][] b = new int[x.length][y.length];  
	        int[][] c = new int[x.length][y.length];  
	          
	        for(int i=1; i<x.length; i++)  
	        {  
	            for(int j=1; j<y.length; j++)  
	            {  
	                //对应第一个性质  
	                if( x[i] == y[j])  
	                {  
	                    c[i][j] = c[i-1][j-1] + 1;  
	                    b[i][j] = 1;  
	                }  
	               
	                else if(c[i-1][j] >= c[i][j-1])  
	                {  
	                    c[i][j] = c[i-1][j];  
	                    b[i][j] = 0; //这个条件与书上的不一样，书上的是2，其实只是一个标号而已，无所谓 
	                }  
	         
	                else  
	                {  
	                    c[i][j] = c[i][j-1];  
	                    b[i][j] = -1;  	//这个条件与书上的不一样，书上的是3，其实只是一个标号而已，无所谓
	                }  
	            }  
	        }   
	        return b;  
	    }  
	    //回溯的基本实现，采取递归的方式  
	    public static void Display(int[][] b, String[] x, int i, int j)  
	    {  
	        if(i == 0 || j == 0)  //终止条件
	            return;  
	          
	        if(b[i][j] == 1)  
	        {  
	            Display(b, x, i-1, j-1);  
	            System.out.print(x[i] + " ");  
	            count++;
	        }  
	        else if(b[i][j] == 0)  //这个条件与书上的不一样，书上的是0，其实只是一个标号而已，无所谓
	        {  
	            Display(b, x, i-1, j);  
	        }  
	        else if(b[i][j] == -1)  //这个条件与书上的不一样，书上的是3，其实只是一个标号而已，无所谓
	        {  
	            Display(b, x, i, j-1);  
	        }  
	    }  
	public static void main(String[] args) {
		//保留空字符串是为了getLength()方法的完整性也可以不保留  
        //但是在getLength()方法里面必须额外的初始化c[][]第一个行第一列  
        String[] x = {"", "A", "B", "C", "B", "D", "A", "B"};  
        String[] y = {"", "B", "D", "C", "A", "B", "A"};  
          
       /* Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();
        String[] s11 = s1.split("");
        String s2 = scanner.nextLine();
        String[] s22 = s2.split("");*/
        
        int[][] b = getLength(x, y);  
          
        Display(b, x, x.length-1, y.length-1); 
        System.out.print("\n");
        System.out.println("LCS.length="+count);
	}

}
