package zeng.glogo.learn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LuckGraph {

	public static void main(String[] args) {
		String sysInputFile = "D://text1.txt";
		try {
			File file = new File(sysInputFile);
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line;
			int array[] = new int[100000];
			int index = 0;
			boolean mark = true;
			while ((line = in.readLine()) != null) {
				String tempStr[] = line.split(" ");
				if (!tempStr[0].equals("")) {
					array[index++] = Integer.parseInt(tempStr[0]);
					array[index++] = Integer.parseInt(tempStr[1]);
					for (int j = 2; j < index; j += 2) {
						if (array[j] == array[j++]) {
							mark = false;
						} else {
							int[] temp = new int[2];
							temp[0] = array[j];
							temp[1] = array[j + 1];
							for (int k = 2; k < index; k += 2) {
								if (k == j)
									break;
								else {
									if (temp[0] == array[k]
											&& temp[1] == array[k + 1]) {
										mark = false;
										break;
									} else if (temp[0] == array[k + 1]
											&& temp[1] == array[k]) {
										mark = false;
										break;
									}
								}
								if (!mark)
									break;
							}
						}
						if (!mark)
							break;
					}
					if (!mark) {
						System.out.println("No");
						break;
					}

				}
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
