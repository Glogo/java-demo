package zeng.glogo.learn;

import java.util.Scanner;

public class FindMins {

	static int n=0;
	static int k=0;
	static int array[];
	
	/*
     *quick sort
     */
    public void QuickSort(){
        QuickSort(0,n-1);
    }
     
     private void QuickSort(int left,int right){
		if (left < right){
			int j = Partition(left, right);
			QuickSort(left, j - 1);
			QuickSort(j + 1, right);
		}
	}
     
     private void Swap(int i, int j){
         int c = array[i];
         array[i] = array[j];
         array[j] = c;
     }
     
     private int Partition(int left,int right){
         int i = left, j = right +1;
         do{
             do i++ ;  while(array[i]<array[left]);
             do j--;   while(array[j]>array[left]);
             if (i<j)   Swap(i,j);
       }while (i<j);
       Swap(left,j);
       return j;
    }

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//Scanner scanner = new Scanner("8 4 4 5 1 6 2 7 3 8");
		do{
			n = scanner.nextInt();
		}while(n>200000);
		
		do{
			k = scanner.nextInt();
		}while(k>n);
		
		array = new int[n];
		for(int i=0; i<array.length; i++){
			array[i]=scanner.nextInt();
		}
		FindMins f = new FindMins();
		f.QuickSort();
		for(int i=0;i<k;i++)
			System.out.print(array[i]+" ");
		scanner.close();
	}
}
