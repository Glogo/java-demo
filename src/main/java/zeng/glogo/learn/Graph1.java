package zeng.glogo.learn;

import java.util.Stack;
import java.util.Vector;

public class Graph1 {

	int vertexNum;
	Vector<Integer>[] vector;	//用来构造图
	Stack<Integer> stack;		//
	int[] result;	//拓扑排序结果
	int[] in;	//入度
	
	/**
	 * 构造一个图
	 */
	@SuppressWarnings("unchecked")
	public Graph1(int num) {
		vertexNum = num;
		vector = new Vector[vertexNum];
		stack = new Stack<Integer>();
		result = new int[vertexNum];
		in = new int[vertexNum];
	}
	
	/**
	 * 向无向图加边
	 */
	public boolean addEdge(int i, int j){
		/** 
         * 判断用户输入的是否是一个顶点，如果是，则返回false,添加不成功 
         */  
        if (j == i) {  //无向图，无回路，无自回路
            return false;  
        }  
        if (i < vertexNum && j < vertexNum && i >= 0 && j >= 0) {  
        	  
            /** 
             *  
             * 判断边是否存在 
             */  
            if (isEdgeExists(i, j))
                return false;  
  
            /** 
             * 添加边，将孤头的入度加1 
             */  
            vector[i].add(j);  
            in[j]++;  		//节点j的入度
            return true;  
        }  
		
		return false;
	}
	
	/**
	 * 计算每个结点入度
	 */
	public void CalInDegree(int[] in){
		for(int i = 0; i < in.length; i++)	
			in[i] = 0;	//先置0
		for(int i = 0; i < in.length ; i++){
			Vector<Integer> p = vector[i];
			if(p != null){
				for(int j = 0; j <p.size() ;j++)
					in[p.get(j)]++;
			}
		}	//end for
	}
	
	
	/** 
     * 判断有向边是否存在 
     *  
     * @param i  要查询的有向边的一个孤尾 
     * @param j  要查询的有向边的另一个孤头 
     * @return 边是否存在，false:不存在，true:存在 
     */  
	public boolean isEdgeExists(int i, int j) {  
		  
        /** 
         * 判断所输入的顶点值是否在图所顶点范围值内，如果不在，则提示顶点不存在 
         *  
         */  
        if (i < vertexNum && j < vertexNum && i >= 0 && j >= 0) {  
  
            if (i == j) {  
                return false;  
            }  
  
            /** 
             * 判断i的邻接结点集是否为空 
             */  
            if (vector[i] == null) {  
                vector[i] = new Vector<Integer>(8);  
            }  
  
            /** 
             * 判断这条边是否存在，如果存在，则提示边已经存在 
             */  
            for (int q = 0; q < vector[i].size(); q++)  
                if (((Integer) vector[i].get(q)).intValue() == j) {  
                    System.out.println("顶点" + i + "和" + "顶点" + j + "这两点之间存在边");  
                    return true;  
                }    
        }  
        return false;  
    }  
	
	
	
	/**
	 * 拓扑排序
	 */
	public void TopSort(){
		for(int i=0; i<vertexNum; i++)
			if(in[i] == 0)
				stack.push(i);	//先把入度为0的节点放入堆栈中
		int k = 0;		//把第一个不为0的结点弹出
		while(!stack.isEmpty()){
			result[k] = stack.pop();	//result存放的是结果
			if(vector[result[k]] != null){
				for(int j=0; j<vector[result[k]].size(); j++){
					int temp = (int) vector[result[k]].get(j);
					for(int m = 0;m<in.length;m++)
						if(temp == in[m])	{
							System.out.println("存在有向回路！");	return;
						}
					if(--in[temp] == 0)
						stack.push(temp);
				}
			}
			
			k++;
		}
	}
	
	public static void main(String[] args) {
		Graph1 graph1 = new Graph1(6);
		graph1.addEdge(1, 0);  
        graph1.addEdge(2, 0);  
        graph1.addEdge(3, 0);  
        graph1.addEdge(1, 3);  
        graph1.addEdge(2, 3);  
        graph1.addEdge(3, 5);  
        graph1.addEdge(4, 2);  
        graph1.addEdge(4, 3);  
        graph1.addEdge(4, 5); 
        graph1.TopSort();
        System.out.println("拓扑排序结果为：");
        for(int i:graph1.result)
        	System.out.print(i+" ");
	}

}
