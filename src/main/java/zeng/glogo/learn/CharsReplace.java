package zeng.glogo.learn;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class CharsReplace {

	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(new BufferedInputStream(System.in));
		String s1 = scanner.nextLine();
		String[] s2 = s1.split("");
		if(s1 != null && s2 !=null)
		{
			for(String t:s2)
			{
				if(t.hashCode()>=65 &&t.hashCode()<=90)
					System.out.print(t.toLowerCase());
				else if(t.hashCode()>=97 && t.hashCode()<=122)
					System.out.print(t.toUpperCase());
				else
					System.out.print(t);
			}
			
		}
		
		scanner.close();
	}

}
