package zeng.glogo.learn;

public class KMPTest {

	int[] f;
	
	public void CalcuFail(String mode){
		f = new int[mode.length()];
		int j = 0;
		int k = -1;
		f[0] = -1;
		while(j < mode.length()-1){
			if((k == -1) || ((mode.charAt(j) == mode.charAt(k) ))){
				j++;	k++;
				f[j] = k;
			}
			else
				k = f[k];
		}
		
	}
	
	public int FindKMP(String source, String mode){
		int i = 0, j = 0;
		CalcuFail(mode);
		while(i < source.length()){
			if(j == -1 || source.charAt(i) == mode.charAt(j)){
				i++;	j++;
			}
			else{
				j = f[j];
			}
			//当j已经匹配到了mode川的末尾，表示已经结束了匹配过程。
			if(j == mode.length())
				return i - mode.length();
		}
		return -1;
	}
	
	
	public static void main(String[] args) {
		String source = "你我他你我他你我他我是水水是我";
		String mode = "我是水";
		
		KMPTest kmp = new KMPTest();
		System.out.println(kmp.FindKMP(source, mode));
		
	}

}
