package zeng.glogo.learn;

public class TreeNode {
	private TreeBean data;
	private TreeNode leftNode;
	private TreeNode rightNode;

	// ���캯��
	public TreeNode() {
		data = new TreeBean();
	}

	public TreeBean getData() {
		return data;
	}

	public void setData(TreeBean data) {
		this.data = data;
	}

	public TreeNode getLeftNode() {
		return leftNode;
	}

	public void setLeftNode(TreeNode leftNode) {
		this.leftNode = leftNode;
	}

	public TreeNode getRightNode() {
		return rightNode;
	}

	public void setRightNode(TreeNode rightNode) {
		this.rightNode = rightNode;
	}
}
